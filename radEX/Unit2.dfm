object dm: Tdm
  OldCreateOrder = False
  Height = 518
  Width = 595
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\mrily\OneDrive\'#1056#1072#1073#1086#1095#1080#1081' '#1089#1090#1086#1083'\radEX\BLINCHIKI.fd' +
        'b'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    Left = 344
    Top = 280
  end
  object quMenu: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image,'
      '    product.gram,'
      '    product.kcal,'
      '    product.price,'
      '    product.note,'
      '    product.sticker'
      'from product')
    Left = 360
    Top = 128
    object quMenuID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quMenuCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quMenuNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 70
    end
    object quMenuIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object quMenuGRAM: TIntegerField
      FieldName = 'GRAM'
      Origin = 'GRAM'
    end
    object quMenuKCAL: TIntegerField
      FieldName = 'KCAL'
      Origin = 'KCAL'
    end
    object quMenuPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quMenuNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quMenuSTICKER: TIntegerField
      FieldName = 'STICKER'
      Origin = 'STICKER'
    end
  end
end
