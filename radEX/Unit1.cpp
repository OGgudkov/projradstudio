//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMenuClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStyleClick(TObject *Sender)
{
	tc->ActiveTab = tiThemes;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buLightClick(TObject *Sender)
{
	fm->StyleBook = StyleLight;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDarkClick(TObject *Sender)
{
    fm->StyleBook = StyleDark;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::ListView1Change(TObject *Sender)
{
    tc->GotoVisibleTab(tiBlin->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button3Click(TObject *Sender)
{
    tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

