//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiMenu;
	TLabel *Label1;
	TImage *Image1;
	TButton *buMenu;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buVictor;
	TButton *buKorz;
	TButton *buConnect;
	TButton *buDelivery;
	TButton *buStyle;
	TButton *buContacts;
	TToolBar *ToolBar1;
	TListView *ListView1;
	TButton *buBack;
	TTabItem *tiThemes;
	TGridPanelLayout *GridPanelLayout2;
	TToolBar *ToolBar2;
	TButton *Button1;
	TButton *buLight;
	TButton *buDark;
	TStyleBook *StyleLight;
	TStyleBook *StyleDark;
	TTabItem *tiBlin;
	TLabel *Label2;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TToolBar *ToolBar3;
	TButton *Button2;
	TImage *Image2;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLabel *laArt;
	TLabel *laCal;
	TLabel *laPrice;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *laVes;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLabel *laDesc;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TLabel *Label7;
	TLabel *Label8;
	TButton *buBucket;
	TButton *buFastOrder;
	TLabel *Label9;
	TLinkPropertyToField *LinkPropertyToFieldText6;
	TTabItem *TabItem1;
	TLabel *Label10;
	TToolBar *ToolBar4;
	TButton *Button3;
	TLabel *Label11;
	TLabel *Label12;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buMenuClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buStyleClick(TObject *Sender);
	void __fastcall buLightClick(TObject *Sender);
	void __fastcall buDarkClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall ListView1Change(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
