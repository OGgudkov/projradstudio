//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Memo.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiCategory;
	TTabItem *tiShort;
	TTabItem *tiAlcohol;
	TTabItem *tiDoctors;
	TBindingsList *BindingsList1;
	TBindSourceDB *BindSourceDB1;
	TBindSourceDB *BindSourceDB2;
	TToolBar *ToolBar1;
	TLabel *Label1;
	TToolBar *ToolBar2;
	TLabel *Label2;
	TTabItem *tiMenu;
	TToolBar *ToolBar3;
	TLabel *Label3;
	TListView *ListView3;
	TToolBar *ToolBar4;
	TLabel *Label4;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField2;
	TBindSourceDB *BindSourceDB4;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buCategory;
	TButton *buInfo;
	TButton *buOptions;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TTabItem *tiOptions;
	TToolBar *ToolBar5;
	TButton *Button5;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buDstyle;
	TButton *buLstyle;
	TStyleBook *DarkStyle;
	TListView *ListView2;
	TLinkListControlToField *LinkListControlToField5;
	TListView *ListView1;
	TLinkListControlToField *LinkListControlToField6;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TStyleBook *LightStyle;
	TStyleBook *BlueStyle;
	TStyleBook *GreenStyle;
	TStyleBook *PurpleStyle;
	TStyleBook *GrayStyle;
	TLabel *Label8;
	TGridPanelLayout *GridPanelLayout3;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TTabItem *tiRadio;
	TToolBar *ToolBar6;
	TButton *Button13;
	TButton *Button14;
	TLabel *Label5;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buCategoryClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall buDstyleClick(TObject *Sender);
	void __fastcall buLstyleClick(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall buOptionsClick(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
   //	void __fastcall Button15Click(TObject *Sender);
	//void __fastcall buDstyleClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
