//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPlayClick(TObject *Sender)
{
	tc->ActiveTab = tiPlay;
    iRoach->Height = 33;
	iRoach->Width = 25;
	iRoach->Position-> X = 384;
	iRoach->Position-> Y = 392;
	FloatAnimation1->Enabled = True;
    laWin->Visible = False;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
    tc->TabPosition = TTabPosition::None;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::iRoachClick(TObject *Sender)
{
	FloatAnimation1->Enabled = False;
	iRoach->Height = 200;
	iRoach->Width = 200;
    laWin->Visible = True;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab = tiLevel;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buHighClick(TObject *Sender)
{
	FloatAnimation1->Duration = 0.5;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buMediumClick(TObject *Sender)
{
    FloatAnimation1->Duration = 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buLowClick(TObject *Sender)
{
	FloatAnimation1->Duration = 3;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	laWin->Visible = False;
	iRoach->Height = 33;
	iRoach->Width = 25;
	iRoach->Position-> X = 384;
	iRoach->Position-> Y = 392;
	FloatAnimation1->Enabled = True;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

