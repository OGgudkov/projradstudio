//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiPlay;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buPlay;
	TImage *iBack;
	TImage *iRoach;
	TFloatAnimation *FloatAnimation1;
	TLabel *laWin;
	TLabel *la1;
	TStyleBook *StyleBook1;
	TButton *Button1;
	TTabItem *tiLevel;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buHigh;
	TButton *buMedium;
	TButton *buLow;
	TToolBar *ToolBar1;
	TButton *Button2;
	TToolBar *ToolBar2;
	TButton *Button3;
	TButton *Button4;
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall iRoachClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buHighClick(TObject *Sender);
	void __fastcall buMediumClick(TObject *Sender);
	void __fastcall buLowClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	//void __fastcall iBackClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
